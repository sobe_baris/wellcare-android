package com.android.wellcare.extensions

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveDataScope
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.afollestad.materialdialogs.MaterialDialog
import com.android.wellcare.data.base.BaseResponse
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import retrofit2.HttpException
import java.net.SocketTimeoutException

internal const val DEFAULT_TIMEOUT = 5000L

class NetworkError(val message: String?)

class NetworkException(
    @SerializedName("message")
    override val message: String?
) : Exception(message)

inline fun tryCatch(tryBlock: () -> Unit, catchBlock: (NetworkError) -> Unit) {
    try {
        tryBlock.invoke()
    } catch (e: HttpException) {
        catchBlock.invoke(NetworkError(e.response()?.errorBody()?.string().orEmpty()))
    }
}

inline fun tryCatch2(tryBlock: () -> Unit, catchBlock: (Exception) -> Unit) {
    try {
        tryBlock.invoke()
    } catch (e: HttpException) {
        val responseBody = e.response()?.errorBody()?.string().orEmpty()
        val networkException: NetworkException =
            Extension.GsonConverter.fromJson(responseBody, NetworkException::class.java)
        catchBlock.invoke(networkException)
    } catch (socketException: SocketTimeoutException) {
        val networkException =
            NetworkException(message = "İşleminizi şu an gerçekleştiremiyoruz.\nLütfen daha sonra tekrar deneyin.")
        catchBlock.invoke(networkException)
    }
}

inline fun <R : BaseResponse> sendNetworkRequest(
    block: () -> Unit,
    returnBlock: (Result<R>) -> Unit
) {
    tryCatch2(block) {
        returnBlock.invoke(Result.failure(it))
    }
}

inline fun <R : BaseResponse> sendNetworkRequest2(
    requestBlock: () -> R,
    returnBlock: (Result<R>) -> Unit
) {
    tryCatch2(
        {
            val response: R = requestBlock.invoke()
            returnBlock.invoke(Result.success(response))
        },
        {
            returnBlock.invoke(Result.failure(it))
        })
}

object Extension {
    val GsonConverter: Gson = Gson()
}

inline fun Fragment.showDialog(block: MaterialDialog.() -> Unit) =
    MaterialDialog(requireActivity()).apply(block).show()

fun <LDM> mutableLiveData(
    block: suspend LiveDataScope<LDM>.() -> Unit
): MutableLiveData<LDM> {
    return liveData(block = block) as MutableLiveData<LDM>
}