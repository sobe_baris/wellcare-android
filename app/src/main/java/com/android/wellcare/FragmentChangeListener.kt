package com.android.wellcare

interface FragmentChangeListener {
    fun onFragmentChanged()
}