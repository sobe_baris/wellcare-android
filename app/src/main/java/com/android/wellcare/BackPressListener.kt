package com.android.wellcare

interface BackPressListener {
    fun onBackPressed()
}