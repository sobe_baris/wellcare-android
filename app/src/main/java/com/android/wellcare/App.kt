package com.android.wellcare

import android.app.Application
import com.android.wellcare.data.account.model.LoginResponse
import com.android.wellcare.di.KoinModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class App : Application() {


    lateinit var loginResponse: LoginResponse

    override fun onCreate() {
        super.onCreate()
        startKoin {
            // Android context
            androidContext(this@App)
            // modules
            modules(koinModules())
        }
    }

    private fun koinModules(): List<Module> {
        return listOf(
            KoinModules.NetworkModule.module,
            KoinModules.ViewModelModule.module,
            KoinModules.ApplicationModule.module,
            KoinModules.MapperModule.module
        )
    }
}