package com.android.wellcare.di

import android.content.Context
import androidx.room.Room
import com.android.wellcare.WellCareDatabase
import com.android.wellcare.data.account.AccountApi
import com.android.wellcare.data.account.AccountRepository
import com.android.wellcare.data.account.TokenDao
import com.android.wellcare.data.account.UserDao
import com.android.wellcare.data.mapper.UserProfileMapper
import com.android.wellcare.data.ui.AppData
import com.android.wellcare.ui.main.login.LoginViewModel
import com.android.wellcare.ui.main.splash.SplashViewModel
import com.android.wellcare.ui.user.UserViewModel
import com.android.wellcare.ui.user.profile.ProfileViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object KoinModules {

    object NetworkModule {

        val module = module {
            factory { provideLogInterceptor() }
            factory { provideOkHttpClient(get()) }
            single { provideRetrofit(get()) }

            factory { provideAccountApi(get()) }
            factory { provideAccountRepository(get(), get(), get()) }
        }

        private fun provideLogInterceptor(): HttpLoggingInterceptor {
            return HttpLoggingInterceptor().also {
                it.level = HttpLoggingInterceptor.Level.BODY
            }
        }

        private fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
            return OkHttpClient().newBuilder()
                .addInterceptor(loggingInterceptor)
                .build()
        }

        private fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
            return Retrofit.Builder()
                .baseUrl("https://api.wellcareapp.com")
                .client(okHttpClient)
                .addConverterFactory(
                    GsonConverterFactory.create()
                ).build()
        }

        private fun provideAccountRepository(
            accountApi: AccountApi,
            userDao: UserDao,
            tokenDao: TokenDao
        ): AccountRepository {
            return AccountRepository(accountApi, userDao, tokenDao)
        }

        private fun provideAccountApi(retrofit: Retrofit): AccountApi {
            return retrofit.create(AccountApi::class.java)
        }

    }

    object ViewModelModule {

        val module = module {
            viewModel { LoginViewModel(get()) }
            viewModel { SplashViewModel(get()) }
            viewModel { ProfileViewModel(get()) }
            viewModel { UserViewModel(get(), get()) }
        }

    }

    object ApplicationModule {

        val module = module {
            single { provideDatabase(androidContext()) }
            single { get<WellCareDatabase>().userDao() }
            single { get<WellCareDatabase>().tokenDao() }
        }

        private fun provideDatabase(context: Context): WellCareDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                WellCareDatabase::class.java,
                "wellcare-database"
            ).build()
        }

        private fun provideAppData(): AppData {
            return AppData
        }

    }

    object MapperModule {

        val module = module {
            //single { provideAppData() }
            single { provideUserProfileMapper() }
        }

        private fun provideUserProfileMapper(): UserProfileMapper {
            return UserProfileMapper()
        }
    }

}