package com.android.wellcare.ui.user.profile

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.afollestad.materialdialogs.input.input
import com.android.wellcare.FragmentTransition
import com.android.wellcare.R
import com.android.wellcare.databinding.FragmentProfileBinding
import com.android.wellcare.extensions.showDialog
import com.android.wellcare.ui.user.BaseUserFragment
import com.android.wellcare.ui.user.profile.edit.ProfileEditFragment
import com.android.wellcare.ui.user.profile.qrlist.ProfileQrListFragment
import com.android.wellcare.ui.user.profile.qrlist.QrListFragmentArguments
import com.android.wellcare.ui.user.profile.qrlist.QrModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class ProfileFragment : BaseUserFragment<FragmentProfileBinding>() {

    val profileViewModel by viewModel<ProfileViewModel>()

    override fun isBackVisible(): Boolean = true

    override fun getName(): String = FragmentName

    override fun getLayoutId(): Int = R.layout.fragment_profile

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        profileViewModel.logoutSingleLiveEvent.observe(
            this@ProfileFragment,
            Observer {
                val intent =
                    requireContext().packageManager.getLaunchIntentForPackage(requireContext().packageName)
                intent?.let {
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(it)
                }
            })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val profileModel = arguments?.getParcelable<ProfileModel>(PROFILE_MODEL_KEY)
        profileModel?.let {
            setView(it)
        } ?: AlertDialog.Builder(requireContext())
            .setMessage(R.string.error_message)
            .setPositiveButton(R.string.action_okey) { dialog, _ ->
                dialog.dismiss()
                getBaseActivity().onBackPressed()
            }
            .show()
    }

    private fun setView(profileModel: ProfileModel) {
        setPageTitle(profileModel.toolbarTitle)
        binding.profileModel = profileModel
        binding.executePendingBindings()

        binding.buttonEditProfile.setOnClickListener {
            val profileEditFragment = ProfileEditFragment.newInstance(profileModel)
            getBaseActivity().replaceFragment(
                profileEditFragment,
                transition = FragmentTransition.createSlideTransition()
            )
        }

        binding.buttonWellcareQR.setOnClickListener {
            val profileQrListFragment = ProfileQrListFragment.newInstance(createQrList())
            getBaseActivity().replaceFragment(
                profileQrListFragment,
                transition = FragmentTransition.createSlideTransition()
            )
        }

        binding.buttonChangePassword.setOnClickListener {
            showDialog {
                title(R.string.profile_change_password)
                input(
                    inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD,
                    maxLength = 12
                ) { _, charSequence -> onReceivedPassword(charSequence) }
                positiveButton(R.string.action_okey)
            }
        }

        binding.buttonLogout.setOnClickListener {
            showDialog {
                title(R.string.logout_title)
                message(R.string.logout_message)
                positiveButton(R.string.action_okey, click = {
                    profileViewModel.logout()
                })
                negativeButton(R.string.action_cancel)
            }
        }
    }

    private fun createQrList(): QrListFragmentArguments {
        val calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH) + 1
        val year = calendar.get(Calendar.YEAR)
        val readableDate = day.toString() + "." + month + "." + year % 100
        val qrModel1 = QrModel(R.drawable.discover_image_3, "Provim ORS", readableDate)
        val qrModel2 = QrModel(R.drawable.discover_image_8, "WellCare Vitamin D3", readableDate)
        val qrModel3 = QrModel(R.drawable.discover_image_5, "WellCare Immune", readableDate)
        return QrListFragmentArguments(mutableListOf(qrModel1, qrModel2, qrModel3))
    }

    private fun onReceivedPassword(password: CharSequence) {

    }

    companion object {
        const val FragmentName = "Profile"
        const val PROFILE_MODEL_KEY = "ProfileModel"

        fun newInstance(profileModel: ProfileModel): ProfileFragment {
            val bundle = Bundle().also { it.putParcelable(PROFILE_MODEL_KEY, profileModel) }
            return ProfileFragment().apply { arguments = bundle }
        }
    }
}