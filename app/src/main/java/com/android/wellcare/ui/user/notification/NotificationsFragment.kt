package com.android.wellcare.ui.user.notification

import android.os.Bundle
import android.view.View
import com.android.wellcare.R
import com.android.wellcare.databinding.FragmentNotificationsBinding
import com.android.wellcare.ui.user.BaseUserFragment

class NotificationsFragment : BaseUserFragment<FragmentNotificationsBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_notifications
    }

    override fun getName(): String {
        return "Notifications"
    }

    override fun showToolbarIcon(): Boolean {
        return false
    }

    override fun isBackVisible(): Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setPageTitle(getString(R.string.page_title_notification))
        val notificationsList = createNotificationList()
        binding.recyclerViewNotifications.adapter =
            NotificationsAdapter(notificationsList) { position ->
                onNotificationClick(position)
            }
    }

    private fun onNotificationClick(position: Int) {
    }

    private fun createNotificationList(): List<Notification> {
        val notification1 = Notification(
            "Title1",
            "NotificationMessage1",
            "9d"
        )
        val notification2 = Notification(
            "NotificationTitle2",
            "Message2",
            "3h"
        )
        return listOf(notification1, notification2)
    }
}