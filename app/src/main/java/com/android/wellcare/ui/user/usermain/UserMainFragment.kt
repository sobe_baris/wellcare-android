package com.android.wellcare.ui.user.usermain

import android.os.Bundle
import android.view.View
import com.android.wellcare.R
import com.android.wellcare.SharedTransition
import com.android.wellcare.databinding.FragmentUserMainBinding
import com.android.wellcare.ui.user.BaseUserFragment
import com.android.wellcare.ui.user.product.ProductFragment

class UserMainFragment : BaseUserFragment<FragmentUserMainBinding>() {

    private val discoverAdapter: DiscoverAdapter by lazy { DiscoverAdapter() }

    override fun getLayoutId(): Int {
        return R.layout.fragment_user_main
    }

    override fun getName(): String = FragmentName

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setPageTitle(getString(R.string.user_wellcare_toolbar_title))

        discoverAdapter.discoverList = generateMockList()
        binding.recyclerViewDiscover.adapter = discoverAdapter
        discoverAdapter.clickListener = {
            val clickedDiscover = discoverAdapter.discoverList[it]
            val productFragment = ProductFragment.newInstance(clickedDiscover)
            getBaseActivity().replaceFragment(
                productFragment,
                sharedTransition = SharedTransition(
                    binding.recyclerViewDiscover.layoutManager?.findViewByPosition(it)!!,
                    clickedDiscover.drawableRes.toString()
                )
            )
            // TODO
        }
    }

    private fun generateMockList(): List<DiscoverModel> {
        val discoverList = mutableListOf<DiscoverModel>()
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_2,
                "Provim IBS sindirim sistemini düzenlemeye ve bağışıklık sistemini desteklemeye yardımcı olur.\n" +
                        "\n" +
                        "İçerik Bilgisi\n" +
                        "Aktif İçerikler\t\n" +
                        "Günlük Kullanım (4 kapsül)\n" +
                        "Lactobacillus Acidophgilus\n" +
                        "4 x 10 9 cfu\n" +
                        "n-bütirik asit\t1920 mg\n" +
                        "Koruyucu, sakkaroz ve glüten içermez.\n" +
                        "\n" +
                        "Yetişkinler için sabah ve akşam olmak üzere iki doza bölünmüş olarak aç karnına günde dört (4) kapsül kullanılması tavsiye edilir. Kapsül çiğnenmeyerek yeterli miktarda su ile yutulmalıdır."
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_3,
                "Provim ORS sindirim sistemini düzenlemeye ve bağışıklık sistemini desteklemeye yardımcı olur. 4-10 yaş grubu çocukların kullanımı için uygundur.\n" +
                        "\n" +
                        "İçerik Bilgisi\n" +
                        "Aktif İçerikler\t\n" +
                        "Günlük Kullanım (1 saşe)\n" +
                        "Probiyotik\t \n" +
                        "Lactobacillus Rhamnosus GG\t\n" +
                        "5 x 10 9 cfu\n" +
                        "ORS İçeriği\t \n" +
                        "Susuz dekstroz\t \n" +
                        "Sodyum Klorür\t \n" +
                        "Tri-potasyum Sitrat Monohidrat\t \n" +
                        "Tri-sodyum Sitrat Dihidrat\t \n" +
                        "PROVIM ORS içerisinde bulunan probiyotik ve ORS çözeltisini iki ayrı odacıktan oluşan inovatif saşe formu DUOCAM® ile tek saşede sunarak kullanım kolaylığı sağlar.\n" +
                        "\n" +
                        " Kullanım önerisi\n" +
                        "Çocuklarda:\n" +
                        "Günde 1 saşe 200 ml su içerisinde çözündürülerek kullanılır. Ahududu aroması ile içimi kolaydır.\n" +
                        "\n" +
                        "Yetişkinlerde:\n" +
                        "Günde 2 saşe olarak kullanımı önerilmektedir."
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_4,
                "Wellcare Allergy Göz Damlası; gözlerde kaşıntı, kızarıklık, sulanma gibi alerjik konjonktivitin semptomlarının önlenmesinde tedaviye yardımcı olur.\n" +
                        "\n" +
                        "İçerik\n" +
                        "İçeriğindeki doğal hücre koruyucu bir molekül olan Ectoin® içermektedir.\n" +
                        "\n" +
                        "Neden Güvenli?\n" +
                        "Wellcare Allergy Göz Damlası, Sağlık Bakanlığı bünyesinde bulunan Türkiye İlaç ve Tıbbi Cihaz Kurumu'ndan onaylı ve tıbbı cihaz kategorisinde bir üründür. %100 doğal bileşenlere dayanan Wellcare Allergy Göz Damlası koruyucu içermemektedir.\n" +
                        "\n" +
                        "6 yaş ve üzeri kullanım için uygundur.\n" +
                        "\n" +
                        "Neden Pratik?\n" +
                        "Wellcare Allergy Göz Damlası açıldıktan sonra tekrar kapatılabilen 0,5 ml flakonlarla kullanıma sunulmuştur.\n" +
                        "\n" +
                        "Açılan flakon göze uygulandıktan sonra uygun hijyen koşulları altında ağzı kapalı olarak muhafaza edildiği taktirde 12 saate kadar tekrar kullanılabilir."
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_5,
                "Wellcare Immune Efervesan Tablet takviye edici gıdadır\n" +
                        "*C vitamini yorgunluk ve bitkinliğin azalmasına katkıda bulunur.\n" +
                        "*C vitamini normal enerji oluşum mekanizmasına katkıda bulunur.\n" +
                        "*C vitamini ve Çinko bağışıklık sisteminin normal fonksiyonuna katkıda bulunur.\n" +
                        "İçindekiler\n" +
                        "Pelargonium Sidoides Kökü Ekstresi, Çinko ve C Vitamini\n" +
                        "Neden Güvenli?\n" +
                        "Alkol içermez\n" +
                        "Kullanım Şekli\n" +
                        "\n" +
                        " 4-10 yaş grubu çocuklar için günde 1 efervesan tablet, 11 yaş ve üstü yetişkinlerde günde 2 efervesan tablet kullanılması tavsiye edilir."
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_6,
                "Wellcare Expera Bitkisel Ekstreler içeren Sıvı takviye edici gıdadır.\n" +
                        "İçerik\n" +
                        "Kekik Ekstresi,Hatmi çiçeği,Propolis Ekstresi\n" +
                        "\n" +
                        "Neden Güvenli?\n" +
                        "Şeker ve renklendirici içermez.\n" +
                        "\n" +
                        "4-10 yaş grubu çocukların kullanımı için uygundur.\n" +
                        "\n" +
                        "Günlük alım dozunda etken madde\tmg 7.5/ml\n" +
                        "Hatmi çiçeği kökü ekstresi\t240\n" +
                        "Keksik ekstresi\t180\n" +
                        "Propolis ekstresi\t67.5"
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_7,
                "Wellcare Immune Bitkisel Ekstreler içeren Sıvı takviye edici gıdadır. Bağışıklık sisteminin normal fonksiyonunun korunmasına katkıda bulunur.\n" +
                        "\n" +
                        "Wellcare Immune Pelargonium Sidoides , Propolis ve C vitamini içeriği ile bitkisel bir içeriğe sahiptir.\n" +
                        "\n" +
                        "İçindekiler\n" +
                        "Pelargonium Sidoides, Propolis, C vitamini\n" +
                        "\n" +
                        "Neden Güvenli?\n" +
                        "Şeker ve renklendirici içermez.\n" +
                        "\n" +
                        "4-10 yaş grubu çocukların kullanımı için uygundur.\n" +
                        "\n" +
                        "Günlük alım dozunda etken madde\tmg 15/ml\n" +
                        "Propolis ekstresi\t135.6\n" +
                        "Vitamin C (L-Askorbik asit)\t100\n" +
                        "Pelargonium sidoides kökü ekstresi\t40"
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_8,
                "“D vitamini bağışıklık sisteminin normal fonksiyonuna katkıda bulunur.” D vitamini normal kemiklerin korunmasına katkıda bulunur.” “D vitamini çocuklarda normal büyüme ve kemik gelişimi için gereklidir.”\n" +
                        "\n" +
                        "Neden Etkili?\n" +
                        "\n" +
                        "D vitamininin, D3 vitamini (kolekalsiferol) ve D2 vitamini (ergokalsiferol) adında iki ayrı formu vardır. Vitamin D2 bitkiler tarafından üretilirken, Vitamin D3 insanlarda cilt dokusundan güneşten gelen UVB ışınları sayesinde oluşur."
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_9,
                "“D vitamini bağışıklık sisteminin normal fonksiyonuna katkıda bulunur.” D vitamini normal kemiklerin korunmasına katkıda bulunur.” “D vitamini çocuklarda normal büyüme ve kemik gelişimi için gereklidir.”\n" +
                        "\n" +
                        "Neden Etkili?\n" +
                        "\n" +
                        "D vitamininin, D3 vitamini (kolekalsiferol) ve D2 vitamini (ergokalsiferol) adında iki ayrı formu vardır. Vitamin D2 bitkiler tarafından üretilirken, Vitamin D3 insanlarda cilt dokusundan güneşten gelen UVB ışınları sayesinde oluşur.\n" +
                        "\n" +
                        "Neden Pratik?\n" +
                        "\n" +
                        "WELLCARE VİTAMİN D3, sıvı şeklinde ve sprey dozaj formunda kullanıma sunulmuştur. 400 IU-600 IU-1000 IU doz seçenekleri ile kullanım kolaylığı sağlar."
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_10,
                "Provim Kids Fast Melt, çocukların sindirim sistemini düzenlemeye ve bağışıklık sistemini desteklemeye yardımcı olur.\n" +
                        "\n" +
                        "C vitamini yorgunluk ve bitkinliğin azalmasına katkıda bulunur.\n" +
                        "C vitamini ve Çinko bağışıklık sisteminin normal fonksiyonuna katkıda bulunur.\n" +
                        "Nasıl Kullanılır\n" +
                        "Paket açıldıktan sonra direkt ağıza dökülerek tüketilmelidir.\n" +
                        "Tükürük sıvısında hızla eriyerek bağırsaklarda optimum sayıda kolonize olması sağlanır.\n" +
                        "Su ve gıdalar ile karıştırılarak tüketilmemelidir.\n" +
                        "Doğal şeftali aroması içermektedir .\n" +
                        "İçerik Bilgisi\n" +
                        "İçindekiler (etken madde)\tGünlük kullanım (1 sase) içerigi %BRD*\t%BRD*\n" +
                        "Lactobacillus rhamnosus BifolacTM GG\n" +
                        "B.A. SSP Lactis BifolacTM 12\n" +
                        "Bifidobacterium Lactis BL-04\t2 x 109 kob/gün\t-\n" +
                        "Vitamin C\t30 mg\t37,5\n" +
                        "Çinko\t5,5 mg\t55\n" +
                        "*% BRD: Beslenme Referans Değeri\n" +
                        "\n" +
                        "Tavsiye Edilen Günlük Alım Dozu\n" +
                        "4-10 yaş grubu çocuklar ile 11 yaş ve üstü yetişkinler için günde 1 saşe kullanılması tavsiye edilir.\n"
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_11,
                "Provim Kids damla, çocukların sindirim sistemini düzenlemeye ve bağışıklık sistemini desteklemeye yardımcı olur.\n" +
                        "4-10 yaş grubu çocukların kullanımına uygundur.\n" +
                        "\n" +
                        "İçerik Bilgisi\n" +
                        "Ayçiçek yağı, Lactobacillus rhamnosus GG ATCC 53103 ve Lactobacillus casei LC03\n" +
                        "\n" +
                        "Günlük alımda etken madde\tGünlük kullanım (5 damla) içeriği\n" +
                        "Lactobacillus rhamnosus GG ATCC 53103\t5,39 mg / 0,5 milyar hücre\n" +
                        "Lactobacillus casei LC03\t5,39 mg / 0,5 milyar hücre\n" +
                        "Ayçiçek yağı\t \n" +
                        "Üretim Teknolojisi – Mikroenkapsülasyon Yöntemi\n" +
                        "Provim Kids içeriğindeki probiyotik mikroorganizmalar Probiotical S.p.A’nın patentli mikroenkapsülasyon teknolojisi ile üretilmiştir. Mikroenkapsülasyon teknolojisi mikroorganizmayı midenin asidik ortamından korur, biyolojik aktivitesini göstereceği bağırsak sistemine güvenle ulaşmasını sağlar ve nem, asidite, osmotik basınç, oksijen ve ışık gibi dış ortamlara karşı koruyarak ürünün raf ömrünü uzatır.\n" +
                        "\n" +
                        "Tavsiye Edilen Günlük Alım Dozu\n" +
                        "4-10 yaş grubu çocuklar için günde 1 defa 5 damla kullanılması tavsiye edilir."
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_12,
                "Provim Daily sindirim sistemini düzenlemeye ve bağışıklık sistemini desteklemeye yardımcı olur. PROVİM DAILY, koruyucu ve gluten içermez. İçeriğinde yer alan probiyotik ve vitamin kombinasyonu doğal yollardan sindirim sistemini ve bağışıklık sistemini desteklemeye yardımcı olur.\n" +
                        "Aktif İçerikler\tGünlük Kullanım (2 kapsül)\n" +
                        "Bacillus Coagulans\t3 x 10 9 cfu\n" +
                        "Lactobacillus Acidophilus\n" +
                        "Lactobacillus Bulgaricus\n" +
                        "Bifidobacterium Lactis\n" +
                        "Stretococcus Thermophilus\n" +
                        "Vitamin C\t80 mg\n" +
                        "Vitamin B3\t10.8 mg\n" +
                        "Vitamin B5\t3,6 mg\n" +
                        "Vitamin B6\t1,2 mg\n" +
                        "Vitamin B2\t0,96 mg\n" +
                        "Vitamin B1\t0,84 mg\n" +
                        "Vitamin B12\t0,6 mcg"
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_13,
                "Wellcare Allergy Burun Spreyi, alerjik rinit belirtilerine karşı güvenli ve etkili bir üründür.\n" +
                        "\n" +
                        "Wellcare Allergy Burun Spreyi, burun mukozasını Aloe vera ve esansiyel yağlar ile ferah ve nemli tutarken, burun mukozası üzerine nazal rinit belirtilerine ( kaşınma, akıntı, aksırma…) neden olan alerjenlerin (polen, mayt, hayvan kılı…) temasını engelleyen koruyucu bir film oluşturacak şekilde formüle edilmiştir.\n" +
                        "\n" +
                        "Etkinliğini arttırmak için, Wellcare Allergy Burun Spreyini önleyici bir yöntem olarak veya ilk belirtiler ortaya çıktığında kullanmak uygundur. Uyku ve sedasyon yapmaz.\n" +
                        "\n" +
                        "İçerik\n" +
                        "Sodyum klorür, ksantan gam, aloe vera, esansiyel yağlar.\n" +
                        "\n" +
                        "Önerilen Doz\n" +
                        "6 yaşından itibaren çocuklar ve yetişkinler için: Günde 3 defaya kadar her bir burun deliğine 1 püskürtme uygulayınız.\n" +
                        "\n" +
                        "Neden Güvenli?\n" +
                        "Etkin doğal bileşenlere dayanan Wellcare Allergy Burun Spreyi, Sağlık Bakanlığı bünyesinde bulunan Türkiye İlaç ve Tıbbi Cihaz Kurumu'ndan onaylı ve tıbbı cihaz kategorisinde bir üründür."
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_14,
                "Gaspass Plus, sindirim sisteminde aşırı gaz birikimine bağlı (şişkinlik, dolgunluk, ağrı, geğirme, gaz çıkarma, vb..) şikâyetleri hızlı ve güvenilir bir şekilde rahatlatan Sağlık Bakanlığı bünyesinde bulunan Türkiye İlaç ve Tıbbi Cihaz Kurumu'ndan onaylı bir üründür.\n" +
                        "\n" +
                        "İçerik\n" +
                        "Her bir tablet;\n" +
                        "50 mg Simetikon\n" +
                        "300 mg Aktif Kömür içermektedir.\n" +
                        "\n" +
                        "Neden Güvenli?\n" +
                        "Gaspass Plus, Sağlık Bakanlığı bünyesinde bulunan Türkiye İlaç ve Tıbbi Cihaz kurumundan onaylı ve tıbbı cihaz kategorisinde bir üründür. Hamilelerde ve 14 yaş altındaki çocuklarda kullanımı yoktur."
            )
        )
        discoverList.add(
            DiscoverModel(
                R.drawable.discover_image_15,
                "Constipass, kabızlık şikâyeti olan çocuk ve yetişkinlerde dışkıyı yumuşatan (laksatif) ve dışkılamayı kolaylaştıran Sağlık Bakanlığı bünyesinde bulunan Türkiye İlaç ve Tıbbi Cihaz Kurumu'ndan onaylı ve tıbbı cihaz kategorisinde bir üründür.\n" +
                        "\n" +
                        "İçerik\n" +
                        "Bileşenler: Her saşe 13,125g Makrogol 3350 içerir.\n" +
                        "\n" +
                        "Yardımcı Maddeler: Silikon dioksit, aroma, sodyum sakkarin.\n" +
                        "\n" +
                        "Neden Pratik?\n" +
                        "Constipass, kullanımı oldukça pratik olan özel bir ambalajda kullanıma sunulmuştur. Bu saşeler birbirinden ayrılabilir ve bölünebilir. Bu sayede daha küçük dozlar elde edilerek farklı yaş grupları için istenilen dozlarda kullanım avantajı sağlar. Her saşe tek kullanımdır ve sadece kullanılacağı zaman açılır. 3 yaş ve üzeri kullanım için uygundur."
            )
        )
        return discoverList
    }

    companion object {
        const val FragmentName = "UserMain"
    }
}