package com.android.wellcare.ui.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.wellcare.data.account.AccountRepository
import com.android.wellcare.data.mapper.UserProfileMapper
import com.android.wellcare.ui.user.profile.ProfileModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel(
    private val accountRepository: AccountRepository,
    private val mapper: UserProfileMapper
) : ViewModel() {

    private val userLiveData: MutableLiveData<ProfileModel> = MutableLiveData<ProfileModel>()

    fun getUserLiveData(): LiveData<ProfileModel> = userLiveData

    fun initialize() {
        viewModelScope.launch {
            val user = accountRepository.getUser()
            user?.let {
                viewModelScope.launch(Dispatchers.Main) {
                    userLiveData.value = mapper.mapFrom(it)
                }
            }
        }
    }
}