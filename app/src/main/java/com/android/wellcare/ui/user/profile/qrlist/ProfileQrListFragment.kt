package com.android.wellcare.ui.user.profile.qrlist

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.android.wellcare.R
import com.android.wellcare.databinding.FragmentProfileQrListBinding
import com.android.wellcare.ui.user.BaseUserFragment

class ProfileQrListFragment : BaseUserFragment<FragmentProfileQrListBinding>() {

    private val qrListAdapter: QrListAdapter by lazy { QrListAdapter() }

    override fun isBackVisible(): Boolean = true

    override fun getName(): String = FragmentName

    override fun getLayoutId(): Int = R.layout.fragment_profile_qr_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val qrListFragmentArguments =
            arguments?.getParcelable<QrListFragmentArguments>(QR_LIST_ARGUMENT_MODEL_KEY)
        qrListFragmentArguments?.let {
            setView(it)
        } ?: AlertDialog.Builder(requireContext())
            .setMessage(R.string.error_message)
            .setPositiveButton(R.string.action_okey) { dialog, _ ->
                dialog.dismiss()
                getBaseActivity().onBackPressed()
            }
            .show()
    }

    private fun setView(qrListFragmentArguments: QrListFragmentArguments) {
        qrListAdapter.qrModelList = qrListFragmentArguments.qrModelList
        binding.recyclerViewQr.adapter = qrListAdapter
        binding.executePendingBindings()
    }

    companion object {
        const val FragmentName = "Profile"
        const val QR_LIST_ARGUMENT_MODEL_KEY = "ProfileModel"

        fun newInstance(qrListFragmentArguments: QrListFragmentArguments): ProfileQrListFragment {
            val bundle = Bundle().also {
                it.putParcelable(
                    QR_LIST_ARGUMENT_MODEL_KEY,
                    qrListFragmentArguments
                )
            }
            return ProfileQrListFragment().apply { arguments = bundle }
        }
    }
}