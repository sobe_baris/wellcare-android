package com.android.wellcare.ui.main

import android.os.Bundle
import com.android.wellcare.ui.base.BaseActivity
import com.android.wellcare.R
import com.android.wellcare.databinding.ActivityMainBinding
import com.android.wellcare.ui.main.splash.SplashFragment

class MainActivity : BaseActivity<ActivityMainBinding>() {

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            initialize()
        } else {
            //  Continue
        }
    }

    private fun initialize() {
        val splashFragment = SplashFragment()
        replaceFragment(splashFragment, false)
    }

    override fun onFragmentChanged() {
        // Do nothing
    }
}
