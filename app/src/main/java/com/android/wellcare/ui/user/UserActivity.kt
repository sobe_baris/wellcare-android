package com.android.wellcare.ui.user

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.android.wellcare.FragmentTransition
import com.android.wellcare.R
import com.android.wellcare.SharedTransition
import com.android.wellcare.Transition
import com.android.wellcare.databinding.ActivityUserBinding
import com.android.wellcare.ui.base.BaseActivity
import com.android.wellcare.ui.base.BaseFragment
import com.android.wellcare.ui.user.barcode.BarcodeFragment
import com.android.wellcare.ui.user.notification.NotificationsFragment
import com.android.wellcare.ui.user.profile.ProfileFragment
import com.android.wellcare.ui.user.profile.ProfileModel
import com.android.wellcare.ui.user.usermain.UserMainFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.koin.androidx.viewmodel.ext.android.viewModel

class UserActivity : BaseActivity<ActivityUserBinding>() {

    val userActivityViewModel by viewModel<UserViewModel>()

    override fun getLayoutId(): Int = R.layout.activity_user

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializeToolbar()
        initializeBottomNavigation()

        @Suppress("ControlFlowWithEmptyBody")
        if (savedInstanceState == null) {
            initialize()
        } else {
            //  Continue
        }
        userActivityViewModel.initialize()
    }

    private fun initializeToolbar() {
        binding.imageViewNotification.setOnClickListener {
            if (it?.visibility == View.VISIBLE) {
                replaceFragment(
                    NotificationsFragment(),
                    transition = FragmentTransition.createFadeTransition()
                )
            }
        }
        binding.imageViewBack.setOnClickListener {
            if (it?.visibility == View.VISIBLE) {
                onBackPressed()
            }
        }
    }

    private fun initializeBottomNavigation() {
        binding.bottomNavigationView.setOnNavigationItemSelectedListener {
            val fragment: BaseFragment? = findNavigationFragmentFromBackstack(it)
            if (fragment != null) {
                if (fragment is BarcodeFragment) {
                    replaceFragment(
                        fragment,
                        false,
                        FragmentTransition.createFadeTransition()
                    )
                } else {
                    replaceFragment(fragment, false)
                }
            } else {
                val navigationFragment = createNavigationFragment(it)
                if (navigationFragment is BarcodeFragment) {
                    replaceFragment(
                        navigationFragment,
                        true,
                        FragmentTransition.createFadeTransition()
                    )
                } else {
                    replaceFragment(navigationFragment, true)
                }

            }

            return@setOnNavigationItemSelectedListener true
        }
    }

    private fun findNavigationFragmentFromBackstack(it: MenuItem): BaseFragment? {
        return when (it.itemId) {
            R.id.navigation_discover -> supportFragmentManager.findFragmentByTag(UserMainFragment.FragmentName) as BaseFragment?
            R.id.navigation_barcode -> supportFragmentManager.findFragmentByTag(BarcodeFragment.FragmentName) as BaseFragment?
            R.id.navigation_profile -> supportFragmentManager.findFragmentByTag(ProfileFragment.FragmentName) as BaseFragment?
            else -> UserMainFragment()
        }
    }

    private fun createNavigationFragment(it: MenuItem): BaseFragment {
        return when (it.itemId) {
            R.id.navigation_discover -> createUserMainFragment()
            R.id.navigation_barcode -> createBarcodeFragment()
            R.id.navigation_profile -> createProfileFragment()
            else -> UserMainFragment()
        }
    }

    private fun createProfileFragment() = ProfileFragment.newInstance(generateProfileModel())

    private fun generateProfileModel(): ProfileModel {
        /*
        return ProfileModel(
            toolbarTitle = "Barış Eczanesi",
            drawableRes = R.drawable.ic_profile_farma,
            owner = "Barış Söbe",
            name = "Barış Eczanesi",
            location = "Pendik / İstanbul"
        )
         */
        return userActivityViewModel.getUserLiveData().value
            ?: throw Exception("User is null on UserActivity")
    }

    private fun createBarcodeFragment() = BarcodeFragment()

    private fun createUserMainFragment() = UserMainFragment()

    private fun initialize() {
        val userMainFragment = UserMainFragment()
        addFragment(userMainFragment, true)
    }

    fun setPageTitle(pageTitle: CharSequence) {
        binding.textViewToolbarTitle.text = pageTitle
    }

    override fun addFragment(fragment: BaseFragment, isAddToBackStack: Boolean) {
        binding.imageViewNotification.visibility =
            if (fragment.showToolbarIcon()) View.VISIBLE else View.INVISIBLE
        super.addFragment(fragment, isAddToBackStack)
    }

    override fun replaceFragment(
        fragment: BaseFragment, isAddToBackStack: Boolean,
        transition: Transition?,
        sharedTransition: SharedTransition?
    ) {
        binding.imageViewNotification.visibility =
            if (fragment.showToolbarIcon()) View.VISIBLE else View.INVISIBLE
        super.replaceFragment(
            fragment,
            isAddToBackStack,
            transition,
            sharedTransition
        )
    }

    override fun onFragmentChanged() {
        val fragment = supportFragmentManager.findFragmentById(R.id.flFragmentContainer)
        if (fragment is BaseFragment) {
            binding.imageViewNotification.visibility =
                if (fragment.showToolbarIcon()) View.VISIBLE else View.INVISIBLE
            if (fragment is BaseUserFragment<*>) {
                isToolbarVisible(fragment.isToolbarVisible())
                isBottomMenuVisible(fragment.isBottomMenuVisible())
                setBackButtonVisibility(fragment.isBackVisible())
                changeBackIconWithClose(fragment.changeBackIconWithClose())
            }
            when (fragment) {
                is UserMainFragment -> {
                    binding.bottomNavigationView.selectWithoutTrigger(R.id.navigation_discover)
                }
                is BarcodeFragment -> {
                    binding.bottomNavigationView.selectWithoutTrigger(R.id.navigation_barcode)
                }
                is ProfileFragment -> {
                    binding.bottomNavigationView.selectWithoutTrigger(R.id.navigation_profile)
                }
                else -> {
                    binding.bottomNavigationView.uncheckAllItems()
                }
            }
        }
    }

    private fun isBottomMenuVisible(isVisible: Boolean) {
        binding.bottomNavigationView.visibility = when (isVisible) {
            true -> View.VISIBLE
            else -> View.GONE
        }
    }

    private fun isToolbarVisible(isVisible: Boolean) {
        binding.toolbar.visibility = when (isVisible) {
            true -> View.VISIBLE
            else -> View.GONE
        }
    }

    private fun setBackButtonVisibility(isVisible: Boolean) {
        binding.imageViewBack.visibility = when (isVisible) {
            true -> View.VISIBLE
            else -> View.INVISIBLE
        }
    }

    private fun changeBackIconWithClose(isVisible: Boolean) {
        binding.imageViewBack.setImageResource(
            when (isVisible) {
                true -> R.drawable.ic_close
                else -> R.drawable.ic_arrow_back
            }
        )
    }

    private fun BottomNavigationView.selectWithoutTrigger(menuId: Int) {
        menu.setGroupCheckable(0, true, false)
        for (i in 0 until menu.size()) {
            val menuItem = menu.getItem(i)
            menuItem.isChecked = menuItem.itemId == menuId
        }
        menu.setGroupCheckable(0, true, true)
    }

    private fun BottomNavigationView.uncheckAllItems() {
        menu.setGroupCheckable(0, true, false)
        for (i in 0 until menu.size()) {
            menu.getItem(i).isChecked = false
        }
        menu.setGroupCheckable(0, true, true)
    }

    fun showTabAtIndex(index: Int) {
        when (index) {
            0 -> binding.bottomNavigationView.selectedItemId = R.id.navigation_discover
            1 -> binding.bottomNavigationView.selectedItemId = R.id.navigation_barcode
            2 -> binding.bottomNavigationView.selectedItemId = R.id.navigation_profile
        }
    }

}
