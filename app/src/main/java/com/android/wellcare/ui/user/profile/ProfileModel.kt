package com.android.wellcare.ui.user.profile

import android.content.Context
import android.os.Parcelable
import androidx.core.content.ContextCompat
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProfileModel(
    val toolbarTitle: String,
    val drawableRes: Int,
    val owner: String? = null,
    val name: String? = null,
    val location: String? = null
) : Parcelable {

    fun getDrawable(context: Context) = ContextCompat.getDrawable(context, drawableRes)
}