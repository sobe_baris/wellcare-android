package com.android.wellcare.ui.user.profile.qrlist

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class QrModel(
    val drawableRes: Int,
    val text: String? = null,
    val date: String? = null
) : Parcelable