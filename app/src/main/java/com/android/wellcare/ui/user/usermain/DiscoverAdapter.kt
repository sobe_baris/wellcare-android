package com.android.wellcare.ui.user.usermain

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.wellcare.R
import com.android.wellcare.databinding.ItemDiscoverBinding

class DiscoverAdapter : RecyclerView.Adapter<DiscoverAdapter.Holder>() {

    lateinit var discoverList: List<DiscoverModel>
    var clickListener: ((Int) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val viewBinding = DataBindingUtil.inflate<ItemDiscoverBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_discover,
            parent,
            false
        )
        return Holder(viewBinding)
    }

    override fun getItemCount(): Int = discoverList.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(discoverList[position])
    }


    inner class Holder(val binding: ItemDiscoverBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                clickListener?.invoke(adapterPosition)
            }
        }

        fun bind(discoverModel: DiscoverModel) {
            ViewCompat.setTransitionName(
                binding.materialCardDiscover,
                discoverModel.drawableRes.toString()
            )
            binding.discoverModel = discoverModel
            binding.executePendingBindings()
        }

    }

}
