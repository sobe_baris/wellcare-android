package com.android.wellcare.ui.user.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.wellcare.SingleLiveEvent
import com.android.wellcare.data.account.AccountRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProfileViewModel(
    private val accountRepository: AccountRepository
) : ViewModel() {

    val logoutSingleLiveEvent: SingleLiveEvent<Any> = SingleLiveEvent()

    fun logout() {
        viewModelScope.launch {
            accountRepository.clearUser()
            accountRepository.clearToken()
            viewModelScope.launch(Dispatchers.Main) {
                logoutSingleLiveEvent.call()
            }
        }
    }
}