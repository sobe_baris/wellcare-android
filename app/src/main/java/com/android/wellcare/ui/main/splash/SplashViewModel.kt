package com.android.wellcare.ui.main.splash

import android.text.TextUtils
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.wellcare.SingleLiveEvent
import com.android.wellcare.data.account.AccountRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SplashViewModel(
    private val accountRepository: AccountRepository
) : ViewModel() {

    val isUserLoggedInLiveEvent: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun isUserLoggedIn() {
        viewModelScope.launch {
            val token = accountRepository.getToken()
            viewModelScope.launch(Dispatchers.Main) {
                val isUserLoggedIn = token?.let {
                    TextUtils.isEmpty(it.token).not()
                } ?: false
                isUserLoggedInLiveEvent.value = isUserLoggedIn
            }
        }
    }

}