package com.android.wellcare.ui.user.barcode

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.core.content.PermissionChecker
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.android.wellcare.R
import com.android.wellcare.databinding.FragmentBarcodeBinding
import com.android.wellcare.extensions.showDialog
import com.android.wellcare.ui.user.BaseUserFragment
import com.android.wellcare.ui.user.UserActivity
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CaptureManager

class BarcodeFragment : BaseUserFragment<FragmentBarcodeBinding>() {

    override fun changeBackIconWithClose(): Boolean = true
    override fun isBackVisible(): Boolean = true
    override fun isBottomMenuVisible(): Boolean = false
    override fun showToolbarIcon(): Boolean = false

    override fun getName(): String = FragmentName

    override fun getLayoutId(): Int = R.layout.fragment_barcode

    private val capture: CaptureManager by lazy {
        CaptureManager(
            requireActivity(),
            binding.decoratedBarcodeView
        )
    }
    private var lastText: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setPageTitle(getString(R.string.page_title_barcode))
        setBarcodeView()
        if (checkSelfPermission(
                requireContext(),
                android.Manifest.permission.CAMERA
            ) != PermissionChecker.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(android.Manifest.permission.CAMERA), CAMERA_PERMISSION_CODE)
        }
    }

    private fun setBarcodeView() {
        binding.decoratedBarcodeView.decodeSingle(object : BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                if (result?.text?.equals(lastText) != false) {
                    // Prevent duplicate scans
                    return
                }

                onBarcodeRead(result)
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
            }

        })
    }

    private fun onBarcodeRead(result: BarcodeResult) {
        lastText = result.text
        showResultDialog(lastText)
        pausePreview()
    }

    private fun showResultDialog(result: String?) {
        showDialog {
            customView(R.layout.layout_barcode_result)
            (getCustomView() as BarcodeResultView).setBarcodeResult(result)
            setOnDismissListener {
                (getBaseActivity() as UserActivity).showTabAtIndex(0)
            }
            cornerRadius(res = R.dimen.dialog_radius)
        }
    }

    private fun startPreview() {
        binding.decoratedBarcodeView.resume()
    }

    private fun pausePreview() {
        binding.decoratedBarcodeView.pause()
    }

    private fun showPermissionDeniedMessage() {
        showDialog {
            title(R.string.camera_permission_denied_title)
            message(R.string.camera_permission_denied_message)
            setOnDismissListener { (getBaseActivity() as UserActivity).showTabAtIndex(0) }
        }
    }

    override fun onResume() {
        super.onResume()
        startPreview()
    }

    override fun onPause() {
        super.onPause()
        pausePreview()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults.getOrNull(0) == PermissionChecker.PERMISSION_GRANTED) {
                startPreview()
            } else {
                showPermissionDeniedMessage()
            }
        }
    }

    companion object {
        const val FragmentName = "Barcode"
        const val CAMERA_PERMISSION_CODE = 123
    }
}