package com.android.wellcare.ui.user.product

import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.transition.TransitionInflater
import com.android.wellcare.R
import com.android.wellcare.databinding.FragmentProductBinding
import com.android.wellcare.ui.user.BaseUserFragment
import com.android.wellcare.ui.user.usermain.DiscoverModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target


class ProductFragment : BaseUserFragment<FragmentProductBinding>() {

    override fun isBackVisible(): Boolean = false
    override fun showToolbarIcon(): Boolean = false

    override fun getLayoutId(): Int {
        return R.layout.fragment_product
    }

    override fun getName(): String = FragmentName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition =
                TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setPageTitle(getString(R.string.product_page_title))
        val discoverModel = requireArguments().getParcelable<DiscoverModel>(DISCOVER_MODEL_KEY)
        ViewCompat.setTransitionName(binding.materialCardProduct, discoverModel?.drawableRes.toString())
        binding.imageViewClose.setOnClickListener { getBaseActivity().onBackPressed() }
        binding.discoverModel = discoverModel

        startPostponedEnterTransition()
        Glide.with(requireContext())
            .load(discoverModel?.drawableRes)
            .dontAnimate()
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    startPostponedEnterTransition()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    startPostponedEnterTransition()
                    return false
                }
            })
            .into(binding.imageViewProduct)
        binding.executePendingBindings()
    }

    companion object {
        const val FragmentName = "Discover"
        const val DISCOVER_MODEL_KEY = "DiscoverModel"

        fun newInstance(discoverModel: DiscoverModel): ProductFragment {
            val bundle = Bundle().also { it.putParcelable(DISCOVER_MODEL_KEY, discoverModel) }
            return ProductFragment().apply { arguments = bundle }
        }
    }
}