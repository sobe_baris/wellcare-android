package com.android.wellcare.ui.main

import android.os.Bundle
import android.view.View
import com.android.wellcare.ui.base.BaseBindingFragment
import com.android.wellcare.R
import com.android.wellcare.databinding.FragmentRegisterBinding

class RegisterFragment : BaseBindingFragment<FragmentRegisterBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_register
    }

    override fun getName(): String {
        return "Register"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.ivRegisterBack.setOnClickListener {
            getBaseActivity().onBackPressed()
        }
    }
}