package com.android.wellcare.ui.main.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.android.wellcare.R
import com.android.wellcare.databinding.FragmentLoginBinding
import com.android.wellcare.extensions.showDialog
import com.android.wellcare.ui.base.BaseBindingFragment
import com.android.wellcare.ui.main.RegisterFragment
import com.android.wellcare.ui.user.UserActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BaseBindingFragment<FragmentLoginBinding>() {

    private val loginViewModel by viewModel<LoginViewModel>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_login
    }

    override fun getName(): String {
        return "Login"
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loginViewModel.successLiveEvent.observe(viewLifecycleOwner, Observer {
            startUserMain()
        })
        loginViewModel.errorLiveEvent.observe(viewLifecycleOwner, Observer {
            showDialog {
                message(text = it)
                positiveButton(text = "Tamam")
            }
        })
    }

    private fun startUserMain() {
        val userActivityIntent = Intent(context, UserActivity::class.java)
        startActivity(userActivityIntent)
        getBaseActivity().finish()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnRegister.setOnClickListener {
            showRegisterFragment()
        }
        binding.ivLoginBack.setOnClickListener {
            getBaseActivity().onBackPressed()
        }
        binding.btnLogin.setOnClickListener {
            doLogin()
        }

        binding.btnLoginTest.setOnClickListener {
            loginViewModel.loginTest()
        }
    }

    private fun doLogin() {
        if (binding.editTextLoginUsername.text.isNullOrEmpty().not()) {
            loginViewModel.login(
                binding.editTextLoginUsername.text.toString(),
                binding.editTextLoginPassword.text.toString()
            )
        }
    }

    private fun showRegisterFragment() {
        getBaseActivity().replaceFragment(RegisterFragment())
    }

}