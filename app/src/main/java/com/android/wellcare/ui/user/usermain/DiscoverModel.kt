package com.android.wellcare.ui.user.usermain

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Parcelable
import androidx.core.content.ContextCompat
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DiscoverModel(
    val drawableRes: Int,
    val text: String
) : Parcelable {

    fun getDrawable(context: Context): Drawable? = ContextCompat.getDrawable(context, drawableRes)
}