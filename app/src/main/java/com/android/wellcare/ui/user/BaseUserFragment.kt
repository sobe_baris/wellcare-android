package com.android.wellcare.ui.user

import androidx.databinding.ViewDataBinding
import com.android.wellcare.ui.base.BaseBindingFragment

abstract class BaseUserFragment<B : ViewDataBinding> : BaseBindingFragment<B>() {

    fun setPageTitle(pageTitle: CharSequence) {
        val baseActivity = getBaseActivity()
        require(baseActivity is UserActivity)
        baseActivity.setPageTitle(pageTitle)
    }

    open fun isToolbarVisible(): Boolean = true

    open fun isBottomMenuVisible(): Boolean = true

    open fun isBackVisible(): Boolean = false

    open fun changeBackIconWithClose() = false
}