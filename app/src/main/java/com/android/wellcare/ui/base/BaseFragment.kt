package com.android.wellcare.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.android.wellcare.BackPressListener

abstract class BaseFragment : Fragment() {

    lateinit var backPressListener: BackPressListener

    abstract fun getName(): String

    @LayoutRes
    abstract fun getLayoutId(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayoutId(), container, false)
    }

    fun getBaseActivity(): BaseActivity<*> {
        if (activity is BaseActivity<*>) {
            return activity as BaseActivity<*>
        } else {
            throw Exception("Activity must be BaseActivity")
        }
    }

    fun triggerBackPressed(): Boolean {
        return if (::backPressListener.isInitialized) {
            backPressListener.onBackPressed()
            true
        } else {
            false
        }
    }

    open fun showToolbarIcon(): Boolean {
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBaseActivity().onFragmentChanged()
    }
}
