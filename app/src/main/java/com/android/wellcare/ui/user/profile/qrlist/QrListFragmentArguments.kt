package com.android.wellcare.ui.user.profile.qrlist

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class QrListFragmentArguments(
    val qrModelList: List<QrModel>
) : Parcelable