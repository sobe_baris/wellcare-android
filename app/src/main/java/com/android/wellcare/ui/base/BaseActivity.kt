package com.android.wellcare.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.android.wellcare.FragmentChangeListener
import com.android.wellcare.R
import com.android.wellcare.SharedTransition
import com.android.wellcare.Transition

abstract class BaseActivity<B : ViewDataBinding> : AppCompatActivity(),
    FragmentChangeListener {

    @LayoutRes
    abstract fun getLayoutId(): Int

    lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayoutId())
    }

    open fun replaceFragment(
        fragment: BaseFragment, isAddToBackStack: Boolean = true,
        transition: Transition? = null,
        sharedTransition: SharedTransition? = null
    ) {
        val transaction = supportFragmentManager
            .beginTransaction()
        if (isAddToBackStack) {
            transaction.addToBackStack(fragment.getName())
        }
        sharedTransition?.let {
            transaction.addSharedElement(it.sharedView, it.sharedKey)
        }
        transition?.let {
            transaction.setCustomAnimations(
                transition.enter,
                transition.exit,
                transition.popEnter,
                transition.popExit
            )
        }
        transaction.replace(R.id.flFragmentContainer, fragment, fragment.getName())
            .commitAllowingStateLoss()
    }

    open fun addFragment(fragment: BaseFragment, isAddToBackStack: Boolean = true) {
        val transaction = supportFragmentManager
            .beginTransaction()
        if (isAddToBackStack) {
            transaction.addToBackStack(fragment.getName())
        }
        transaction.add(R.id.flFragmentContainer, fragment, fragment.getName())
            .commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.flFragmentContainer)
        if (fragment is BaseFragment) {
            val isTriggered = fragment.triggerBackPressed()
            if (isTriggered.not()) {
                val isDetached = detachFragmentIfExist()
                if (isDetached.not()) {
                    finish()
                }
            }
        } else {
            val isDetached = detachFragmentIfExist()
            if (isDetached.not()) {
                finish()
            }
        }
    }

    private fun detachFragmentIfExist(): Boolean {
        return if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
            true
        } else {
            false
        }
    }
}
