package com.android.wellcare.ui.user.notification

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.wellcare.R
import com.android.wellcare.databinding.ItemNotificationsBinding

class NotificationsAdapter(
    private val notificationList: List<Notification>,
    private val clickListener: (Int) -> (Unit)
) :
    RecyclerView.Adapter<NotificationsAdapter.Holder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemNotificationsBinding = DataBindingUtil.inflate<ItemNotificationsBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_notifications,
            parent,
            false
        )
        return Holder(
            itemNotificationsBinding
        )
    }

    override fun getItemCount(): Int = notificationList.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(notificationList[position])
        holder.itemView.setOnClickListener {
            clickListener.invoke(holder.adapterPosition)
        }
    }


    class Holder(private val binding: ItemNotificationsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(notification: Notification) {
            binding.notification = notification
            binding.executePendingBindings()
        }

    }
}