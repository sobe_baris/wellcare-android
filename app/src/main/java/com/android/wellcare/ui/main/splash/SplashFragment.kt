package com.android.wellcare.ui.main.splash

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.android.wellcare.R
import com.android.wellcare.ui.base.BaseFragment
import com.android.wellcare.ui.main.login.LoginFragment
import com.android.wellcare.ui.user.UserActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashFragment : BaseFragment() {


    private val splashViewModel by viewModel<SplashViewModel>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_splash
    }

    override fun getName(): String {
        return "Splash"
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        splashViewModel.isUserLoggedInLiveEvent.observe(
            this@SplashFragment, Observer { isUserLoggedIn -> renderNextPage(isUserLoggedIn) })
        splashViewModel.isUserLoggedIn()
    }

    private fun renderNextPage(isUserLoggedIn: Boolean) {
        requireView().postDelayed({
            if (isUserLoggedIn) {
                startUserMain()
            } else {
                startLogin()
            }
        }, DELAY_MILLISECOND)
    }

    private fun startLogin() {
        getBaseActivity().replaceFragment(LoginFragment())
    }

    private fun startUserMain() {
        val userActivityIntent = Intent(context, UserActivity::class.java)
        startActivity(userActivityIntent)
        getBaseActivity().finish()
    }

    companion object {
        const val DELAY_MILLISECOND = 200L
    }
}