package com.android.wellcare.ui.user.profile.edit

import android.os.Bundle
import android.text.InputType
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.afollestad.materialdialogs.input.input
import com.android.wellcare.R
import com.android.wellcare.databinding.FragmentProfileEditBinding
import com.android.wellcare.extensions.showDialog
import com.android.wellcare.ui.user.BaseUserFragment
import com.android.wellcare.ui.user.profile.ProfileModel

class ProfileEditFragment : BaseUserFragment<FragmentProfileEditBinding>() {

    override fun isBackVisible(): Boolean = true

    override fun getName(): String = FragmentName

    override fun getLayoutId(): Int = R.layout.fragment_profile_edit

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val profileModel = arguments?.getParcelable<ProfileModel>(PROFILE_MODEL_KEY)
        profileModel?.let {
            setPageTitle(getString(R.string.profile_edit_profile))
            binding.profileModel = it
            binding.executePendingBindings()
        } ?: AlertDialog.Builder(requireContext())
            .setMessage(R.string.error_message)
            .setPositiveButton(R.string.action_okey) { dialog, _ ->
                dialog.dismiss()
                getBaseActivity().onBackPressed()
            }
            .show()

        binding.buttonChangeName.setOnClickListener {
            showChangeNameDialog()
        }

        binding.buttonChangePharmacyName.setOnClickListener {
            showChangePharmacyNameDialog()
        }

        binding.buttonChangePhoneNumber.setOnClickListener {
            showChangePhoneNumberDialog()
        }
    }

    private fun showChangeNameDialog() {
        showDialog {
            title(res = R.string.profile_edit_change_name)
            input(
                prefill = binding.profileModel?.owner,
                inputType = InputType.TYPE_CLASS_TEXT,
                maxLength = 32
            ) { _, charSequence -> onReceivedName(charSequence) }
            positiveButton(R.string.action_okey)
            negativeButton(R.string.action_cancel)
        }
    }

    private fun onReceivedName(name: CharSequence) {

    }

    private fun showChangePharmacyNameDialog() {
        showDialog {
            title(res = R.string.profile_edit_change_pharmacy_name)
            input(
                prefill = binding.profileModel?.name,
                inputType = InputType.TYPE_CLASS_TEXT,
                maxLength = 32
            ) { _, charSequence -> onReceivedName(charSequence) }
            positiveButton(R.string.action_okey)
            negativeButton(R.string.action_cancel)
        }
    }

    private fun showChangePhoneNumberDialog() {
        showDialog {
            title(res = R.string.profile_edit_change_phone_number)
            input(
                inputType = InputType.TYPE_CLASS_PHONE
            ) { _, charSequence -> onReceivedName(charSequence) }
            positiveButton(R.string.action_okey)
            negativeButton(R.string.action_cancel)
        }
    }

    companion object {
        const val FragmentName = "Profile"
        const val PROFILE_MODEL_KEY = "ProfileModel"

        fun newInstance(profileModel: ProfileModel): ProfileEditFragment {
            val bundle = Bundle().also { it.putParcelable(PROFILE_MODEL_KEY, profileModel) }
            return ProfileEditFragment().apply { arguments = bundle }
        }
    }
}