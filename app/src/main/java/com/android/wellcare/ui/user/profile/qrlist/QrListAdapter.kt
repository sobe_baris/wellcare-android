package com.android.wellcare.ui.user.profile.qrlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.wellcare.R
import com.android.wellcare.databinding.ItemQrBinding

class QrListAdapter : RecyclerView.Adapter<QrListAdapter.Holder>() {

    lateinit var qrModelList: List<QrModel>
    var clickListener: ((Int) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val viewBinding = DataBindingUtil.inflate<ItemQrBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_qr,
            parent,
            false
        )
        return Holder(viewBinding)
    }

    override fun getItemCount(): Int = qrModelList.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(qrModelList[position])
    }


    inner class Holder(val binding: ItemQrBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                clickListener?.invoke(adapterPosition)
            }
        }

        fun bind(qrModel: QrModel) {
            binding.qrModel = qrModel
            binding.executePendingBindings()
        }

    }

}
