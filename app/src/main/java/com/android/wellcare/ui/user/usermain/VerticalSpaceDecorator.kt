package com.android.wellcare.ui.user.usermain

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class VerticalSpaceDecorator(private val verticalSpaceHeight: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val indexOfChild = parent.indexOfChild(view)
        if (indexOfChild == 0) {
            outRect.top = verticalSpaceHeight
        } else {
            outRect.top = 0
        }
        outRect.bottom = verticalSpaceHeight
        outRect.left = verticalSpaceHeight
        outRect.right = verticalSpaceHeight
    }
}