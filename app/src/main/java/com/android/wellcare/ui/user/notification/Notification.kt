package com.android.wellcare.ui.user.notification

data class Notification(
    val title: String,
    val message: String,
    val notificationTime: String
)