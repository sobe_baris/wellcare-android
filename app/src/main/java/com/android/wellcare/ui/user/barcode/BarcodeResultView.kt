package com.android.wellcare.ui.user.barcode

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.android.wellcare.R
import com.android.wellcare.databinding.ViewBarcodeResultBinding

class BarcodeResultView : LinearLayout {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private lateinit var binding: ViewBarcodeResultBinding

    init {
        if (isInEditMode) {
            View.inflate(context, R.layout.view_barcode_result, this)
        } else {
            binding = ViewBarcodeResultBinding.inflate(LayoutInflater.from(context), this, true)
        }
    }

    fun setBarcodeResult(barcodeResult: String?) {
        binding.barcodeResult = barcodeResult
        binding.executePendingBindings()
    }

}
