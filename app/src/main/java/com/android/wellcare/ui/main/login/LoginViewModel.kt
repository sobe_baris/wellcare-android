package com.android.wellcare.ui.main.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.wellcare.SingleLiveEvent
import com.android.wellcare.data.account.AccountRepository
import com.android.wellcare.data.account.model.LoginRequest
import com.android.wellcare.data.account.model.LoginResponse
import com.android.wellcare.data.account.model.Token
import com.android.wellcare.data.account.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel(
    private val accountRepository: AccountRepository
) : ViewModel() {

    val successLiveEvent: SingleLiveEvent<Any> = SingleLiveEvent()
    val errorLiveEvent: SingleLiveEvent<String> = SingleLiveEvent()

    fun login(username: String, password: String) {
        val loginRequest = LoginRequest(username, password)

        viewModelScope.launch {
            accountRepository.login(loginRequest) { loginResponseResult ->
                loginResponseResult.onSuccess { loginResponse ->
                    loginResponse.user?.let { user ->
                        saveUser(user)
                    }
                    loginResponse.token?.let { token ->
                        saveToken(token)
                    }
                    viewModelScope.launch(Dispatchers.Main) {
                        successLiveEvent.call()
                    }
                }.onFailure { exception ->
                    errorLiveEvent.value = exception.message
                }
            }
        }
    }

    fun loginTest() {
        viewModelScope.launch {
            val testUser = User(
                key = 1,
                id = "testId",
                email = "test_mail@test.com",
                username = "testUserName",
                phoneNumber = "05341346643",
                fullname = "Test Account",
                isPharmacist = false,
                createdTime = "",
                modifiedTime = "",
                isAdmin = false,
                totalPoint = 192
            )
            val loginResponse = LoginResponse("testToken", testUser)
            loginResponse.user?.let { user ->
                saveUser(user)
            }
            loginResponse.token?.let { token ->
                saveToken(token)
            }
            viewModelScope.launch(Dispatchers.Main) {
                successLiveEvent.call()
            }
        }
    }

    private fun saveToken(tokenString: String) {
        viewModelScope.launch {
            val token = Token(token = tokenString)
            accountRepository.saveToken(token)
        }
    }

    private fun saveUser(user: User) {
        viewModelScope.launch {
            accountRepository.saveUser(user)
        }
    }

}