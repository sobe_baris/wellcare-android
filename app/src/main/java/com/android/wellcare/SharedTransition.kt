package com.android.wellcare

import android.view.View

class SharedTransition(val sharedView: View, val sharedKey: String)