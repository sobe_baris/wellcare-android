package com.android.wellcare

class Transition(
    val enter: Int,
    val exit: Int,
    val popEnter: Int,
    val popExit: Int
)