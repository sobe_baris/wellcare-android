package com.android.wellcare

object FragmentTransition {

    fun createFadeTransition(): Transition {
        return Transition(
            enter = R.anim.fade_in,
            exit = R.anim.fade_out,
            popEnter = R.anim.fade_in,
            popExit = R.anim.fade_out
        )
    }

    fun createSlideTransition(): Transition {
        return Transition(
            enter = R.anim.slide_in_right,
            exit = R.anim.slide_out_left,
            popEnter = R.anim.slide_in_left,
            popExit = R.anim.slide_out_right
        )
    }
}
