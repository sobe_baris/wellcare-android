package com.android.wellcare

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.wellcare.data.account.TokenDao
import com.android.wellcare.data.account.UserDao
import com.android.wellcare.data.account.model.Token
import com.android.wellcare.data.account.model.User

@Database(
    entities =
    [
        User::class,
        Token::class
    ]
    , version = 1
)
abstract class WellCareDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun tokenDao(): TokenDao

}