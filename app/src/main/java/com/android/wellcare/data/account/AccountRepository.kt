package com.android.wellcare.data.account

import com.android.wellcare.data.account.model.LoginRequest
import com.android.wellcare.data.account.model.LoginResponse
import com.android.wellcare.data.account.model.Token
import com.android.wellcare.data.account.model.User
import com.android.wellcare.extensions.sendNetworkRequest2

class AccountRepository(
    private val accountApi: AccountApi,
    private val userDao: UserDao,
    private val tokenDao: TokenDao
) {

    suspend fun login(
        loginRequest: LoginRequest,
        callback: ((Result<LoginResponse>) -> Unit)
    ) {
        sendNetworkRequest2(
            {
                accountApi.login(loginRequest)
            },
            {
                callback.invoke(it)
            }
        )
    }

    suspend fun getUser(): User? {
        return userDao.getUser().firstOrNull()
    }

    suspend fun saveUser(user: User) {
        userDao.insert(user)
    }

    suspend fun clearUser() {
        userDao.deleteAllUsers()
    }

    suspend fun getToken(): Token? {
        return tokenDao.getToken().firstOrNull()
    }

    suspend fun saveToken(token: Token) {
        tokenDao.insert(token)
    }

    suspend fun clearToken() {
        tokenDao.clearToken()
    }
}

