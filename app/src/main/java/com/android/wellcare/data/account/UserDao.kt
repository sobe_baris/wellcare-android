package com.android.wellcare.data.account

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.android.wellcare.data.account.model.User

@Dao
interface UserDao {

    @Query("SELECT * FROM user")
    suspend fun getUser(): List<User>

    @Query("DELETE FROM user")
    suspend fun deleteAllUsers()

    @Insert
    suspend fun insert(user: User)

    @Delete
    suspend fun delete(user: User)

}