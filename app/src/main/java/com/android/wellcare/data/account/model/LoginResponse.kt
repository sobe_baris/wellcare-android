package com.android.wellcare.data.account.model

import com.android.wellcare.data.base.BaseResponse
import com.google.gson.annotations.SerializedName

class LoginResponse(
    @SerializedName("token")
    val token: String? = null,

    @SerializedName("user")
    val user: User? = null,

    message: String? = null

) : BaseResponse(message)