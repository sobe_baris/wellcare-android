package com.android.wellcare.data.account.model

class LoginRequest(
    val username: String,
    val password: String
)