package com.android.wellcare.data.ui

import com.android.wellcare.data.account.model.User

object AppData {
    var user: User? = null
    var token: String? = null
}