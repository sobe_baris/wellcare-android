package com.android.wellcare.data.account

import com.android.wellcare.data.account.model.LoginRequest
import com.android.wellcare.data.account.model.LoginResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface AccountApi {

    @POST("login")
    suspend fun login(@Body loginRequest: LoginRequest): LoginResponse
}