package com.android.wellcare.data.account.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "user")
class User(

    @PrimaryKey(autoGenerate = true)
    val key: Long,

    @SerializedName("_id")
    val id: String?,

    @SerializedName("_created")
    val createdTime: String?,

    @SerializedName("_modified")
    val modifiedTime: String?,

    @SerializedName("email")
    @ColumnInfo(name = "email")
    val email: String?,

    @SerializedName("username")
    @ColumnInfo(name = "username")
    val username: String?,

    @SerializedName("fullname")
    @ColumnInfo(name = "fullname")
    val fullname: String?,

    @SerializedName("phoneNumber")
    @ColumnInfo(name = "phoneNumber")
    val phoneNumber: String?,

    @SerializedName("isPharmacist")
    @ColumnInfo(name = "isPharmacist")
    val isPharmacist: Boolean?,

    @SerializedName("isAdmin")
    val isAdmin: Boolean?,

    @SerializedName("totalPoint")
    @ColumnInfo(name = "totalPoint")
    val totalPoint: Long?
)