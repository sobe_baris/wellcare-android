package com.android.wellcare.data.base

import com.google.gson.annotations.SerializedName

abstract class BaseResponse(
    @SerializedName("message")
    val message: String? = null
)