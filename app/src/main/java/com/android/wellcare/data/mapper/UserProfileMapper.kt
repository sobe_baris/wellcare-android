package com.android.wellcare.data.mapper

import com.android.wellcare.R
import com.android.wellcare.data.account.model.User
import com.android.wellcare.ui.user.profile.ProfileModel

class UserProfileMapper {

    fun mapFrom(user: User): ProfileModel {
        return ProfileModel(
            toolbarTitle = user.fullname.orEmpty(),
            drawableRes = R.drawable.ic_profile_farma,
            owner = user.fullname,
            name = user.username,
            location = "Data Yok"
        )
    }

}
