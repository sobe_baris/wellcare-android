package com.android.wellcare.data.account

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.android.wellcare.data.account.model.Token

@Dao
interface TokenDao {

    @Query("SELECT * FROM token")
    suspend fun getToken(): List<Token>

    @Query("DELETE FROM token")
    suspend fun clearToken()

    @Insert
    suspend fun insert(token: Token)

    @Delete
    suspend fun delete(token: Token)

}