package com.android.wellcare.data.account.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "token")
class Token(

    @PrimaryKey(autoGenerate = true)
    val key: Long = -1,

    val token: String?
)