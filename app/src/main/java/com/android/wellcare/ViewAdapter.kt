package com.android.wellcare

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.android.wellcare.ui.user.usermain.VerticalSpaceDecorator
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import jp.wasabeef.glide.transformations.RoundedCornersTransformation

@BindingAdapter(value = ["imageLink", "radius"])
public fun setImageLink(imageView: ImageView, imageLink: String, radius : Float) {
    Glide.with(imageView)
        .load(imageLink)
        .apply(RequestOptions.bitmapTransform(RoundedCornersTransformation(radius.toInt(), 0)))
        .into(imageView)
}

@BindingAdapter(value = ["imageResource", "radius"])
public fun setImageResource(imageView: ImageView, imageResource: Int, radius : Float) {
    Glide.with(imageView)
        .load(imageResource)
        .apply(RequestOptions.bitmapTransform(RoundedCornersTransformation(radius.toInt(), 0)))
        .override(600, 600)
        .into(imageView)
}

@BindingAdapter("verticalDividerHeight")
public fun setVerticalDividerHeight(recyclerView: RecyclerView, verticalDividerHeight: Float) {
    recyclerView.addItemDecoration(VerticalSpaceDecorator(verticalDividerHeight.toInt()))
}